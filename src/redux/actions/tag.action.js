import axios from 'axios';
import config from '../../Config';

export const fetchTagsSuccess = (data) => {
  return {
    type: 'FETCH_TAGS_SUCCESS',
    payload: data,
  };
};

export const fetchTags = (offset, limit) => {
  return (dispatch) => {
    return axios
      .get(`${config.api.URL}/tags?offset=${offset}&limit=${limit}`)
      .then((res) => {
        const data = res.data;
        dispatch(fetchTagsSuccess(data));
      })
      .catch((err) => {
        console.error(err);
      });
  };
};

export const saveTagSuccess = (data) => {
  return {
    type: 'SAVE_TAG_SUCCESS',
    payload: data,
  };
};

export const saveTag = (name) => {
  return (dispatch) => {
    return axios
      .post(`${config.api.URL}/tags`, { name })
      .then((res) => dispatch(saveTagSuccess(res.data)))
      .catch((err) => console.error(err));
  };
};

export const editTagSuccess = (data) => {
  return {
    type: 'EDIT_TAG_SUCCESS',
    payload: data,
  };
};

export const editTag = (tagId, name) => {
  return (dispatch) => {
    return axios
      .put(`${config.api.URL}/tags/${tagId}`, { name })
      .then((res) => {
        const data = res.data;
        dispatch(editTagSuccess(data));
      })
      .catch((err) => console.error(err));
  };
};

export const setEditingTagName = (name) => {
  return {
    type: 'SET_EDITING_TAG_NAME',
    payload: name,
  };
};

export const openCreateTagModal = () => {
  return {
    type: 'OPEN_CREATE_TAG_MODAL',
  };
};

export const hideCreateTagModal = () => {
  return {
    type: 'HIDE_CREATE_TAG_MODAL',
  };
};

export const setTagToEdit = (tag) => {
  return {
    type: 'SET_TAG_TO_EDIT',
    payload: tag,
  };
};
