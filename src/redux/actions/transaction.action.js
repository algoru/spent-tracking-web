import axios from 'axios';
import config from '../../Config';

export const fetchTransactionsSuccess = (data) => {
  return {
    type: 'FETCH_TRANSACTIONS_SUCCESS',
    payload: data,
  };
};

export const fetchTransactionsLoading = (data) => {
  return {
    type: 'FETCH_TRANSACTIONS_LOADING',
    payload: data,
  };
};

export const fetchTransactions = (filter) => {
  const url = buildFetchURL(filter);
  return (dispatch) => {
    return axios
      .get(url)
      .then((res) => {
        const data = res.data;
        dispatch(fetchTransactionsSuccess(data));
      })
      .catch((err) => console.error(err));
  };
};

export const editTransactionSuccess = (data) => {
  return {
    type: 'EDIT_TRANSACTION_SUCESS',
    payload: data,
  };
};

export const editTransactionFail = (data) => {
  return {
    type: 'EDIT_TRANSACTION_FAIL',
    payload: data,
  };
};

export const editTransaction = (editedTransaction) => {
  return (dispatch) => {
    return axios
      .put(`${config.api.URL}/transactions/${editedTransaction.id}`, {
        tag_id: editedTransaction.tag.id,
        value: editedTransaction.value,
      })
      .then((res) => {
        const data = res.data;
        if (res.status === 200) {
          dispatch(editTransactionSuccess(data));
        } else {
          dispatch(editTransactionFail(data));
        }
      })
      .catch((err) => console.error(err));
  };
};

export const setTransactionsFilter = (filter) => {
  return {
    type: 'SET_TRANSACTIONS_FILTER',
    payload: filter,
  };
};

export const openTransactionEditModal = (transaction) => {
  return {
    type: 'OPEN_TRANSACTION_EDIT',
    payload: transaction,
  };
};

export const closeTransactionEditModal = (transaction) => {
  return {
    type: 'CLOSE_TRANSACTION_EDIT',
  };
};

export const setEditingTransactionTag = (tagId) => {
  return {
    type: 'SET_EDITING_TRANSACTION_TAG',
    payload: tagId,
  };
};

export const setEditingTransactionValue = (value) => {
  return {
    type: 'SET_EDITING_TRANSACTION_VALUE',
    payload: value,
  };
};

export const openCreateTransactionModal = (incrementing) => {
  return {
    type: 'OPEN_CREATE_TRANSACTION_MODAL',
    payload: incrementing,
  };
};

export const hideCreateTransactionModal = () => {
  return {
    type: 'HIDE_CREATE_TRANSACTION_MODAL',
  };
};

export const saveTransactionSuccess = (data) => {
  return {
    type: 'SAVE_TRANSACTION_SUCCESS',
    payload: data,
  };
};

export const saveTransaction = (tagId, value) => {
  return (dispatch) => {
    return axios
      .post(`${config.api.URL}/transactions`, { tag_id: tagId, value })
      .then((res) => {
        const data = res.data;
        dispatch(saveTransactionSuccess(data));
      })
      .catch((err) => console.error(err));
  };
};

export const deleteTransactionSuccess = (data) => {
  return {
    type: 'DELETE_TRANSACTION_SUCCESS',
    payload: data,
  };
};

export const deleteTransaction = (id) => {
  return (dispatch) => {
    return axios
      .delete(`${config.api.URL}/transactions/${id}`)
      .then((res) => {
        const data = res.data;
        dispatch(deleteTransactionSuccess(data));
      })
      .catch((err) => console.error(err));
  };
};

function buildFetchURL(filter) {
  let url = `${config.api.URL}/transactions?`;
  if (filter.offset !== undefined) {
    url += `offset=${filter.offset}`;
  }
  if (filter.limit) {
    url += `&limit=${filter.limit}`;
  }
  if (filter.tagId) {
    url += `&tagId=${filter.tagId}`;
  }
  if (filter.from) {
    url += `&from=${filter.from}`;
  }
  if (filter.to) {
    url += `&to=${filter.to}`;
  }

  return url;
}
