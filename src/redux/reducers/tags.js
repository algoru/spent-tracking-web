const defaultState = {
  tags: [],
  error: null,
  isLoading: false,
};

export const tagsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'OPEN_CREATE_TAG_MODAL':
      return { ...state, showCreate: true };
    case 'HIDE_CREATE_TAG_MODAL':
      return { ...state, showCreate: false, success: undefined };
    case 'OPEN_EDIT_TAG_MODAL':
      return { ...state, showEditTags: true, success: undefined };
    case 'HIDE_EDIT_TAG_MODAL':
      return { ...state, showEditTags: false, success: undefined };
    case 'SAVE_TAG_SUCCESS':
      return { ...state, tags: [...state.tags, action.payload], success: true };
    case 'FETCH_TAGS_SUCCESS':
      return { ...state, tags: action.payload };
    case 'SET_EDITING_TAG_NAME':
      return { ...state, editingTagName: action.payload };
    case 'SAVE_TAG_FAIL':
      return { ...state, success: false };
    case 'SET_TAG_TO_EDIT':
      return { ...state, editingTag: action.payload };
    case 'EDIT_TAG_SUCCESS':
      const tags = state.tags;
      tags.forEach((tag, index) => {
        if (tag.id === action.payload.id) {
          tags[index] = action.payload;
        }
      });

      return { ...state, tags, updatedTag: action.payload, success: true };
    default:
      return state;
  }
};
