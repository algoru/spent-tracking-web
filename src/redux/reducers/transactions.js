const initialState = { transactions: [], error: null, isLoading: false };

export const transactionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_TRANSACTIONS_SUCCESS':
      return { ...state, transactions: action.payload };
    case 'FETCH_TRANSACTIONS_LOADING':
      return { ...state, isLoading: action.payload };
    case 'OPEN_TRANSACTION_EDIT':
      return {
        ...state,
        editing: action.payload,
        show: true,
        success: undefined,
      };
    case 'CLOSE_TRANSACTION_EDIT':
      return { ...state, editing: null, show: false };
    case 'SET_EDITING_TRANSACTION_TAG':
      return {
        ...state,
        editing: {
          ...state.editing,
          tag: { ...state.editing.tag, id: action.payload },
        },
      };
    case 'SET_EDITING_TRANSACTION_VALUE':
      return { ...state, editing: { ...state.editing, value: action.payload } };
    case 'EDIT_TRANSACTION_SUCESS':
      const txs = state.transactions;
      txs.forEach((tx, i) => {
        if (tx.id === action.payload.id) {
          txs[i] = action.payload;
        }
      });

      return { ...state, success: true, transactions: txs };
    case 'EDIT_TRANSACTION_FAIL':
      return { ...state, success: false };
    case 'OPEN_CREATE_TRANSACTION_MODAL':
      return {
        ...state,
        showCreate: true,
        isDeposit: action.payload,
        success: undefined,
      };
    case 'HIDE_CREATE_TRANSACTION_MODAL':
      return { ...state, showCreate: false, success: undefined };
    case 'SAVE_TRANSACTION_SUCCESS':
      return {
        ...state,
        transactions: [...state.transactions, action.payload],
        success: true,
      };
    case 'SAVE_TRANSACTION_FAIL':
      return { ...state, success: false };
    case 'DELETE_TRANSACTION_SUCCESS':
      let updatedTxs = state.transactions;
      updatedTxs.forEach((tx, index) => {
        if (tx.id == action.payload.id) {
          updatedTxs.splice(index, 1);
        }
      });
      return { ...state, sucess: true, transactions: updatedTxs };
    default:
      return state;
  }
};

export const transactionsFilterReducer = (state = {}, action) => {
  switch (action.type) {
    case 'SET_TRANSACTIONS_FILTER':
      return action.payload;
    default:
      return state;
  }
};
