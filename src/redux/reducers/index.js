import { combineReducers } from 'redux';
import { createTagModalReducer, editTagModalReducer } from './modals';
import { transactionsReducer, transactionsFilterReducer } from './transactions';
import { tagsReducer } from './tags';

const reducers = combineReducers({
  tags: tagsReducer,
  transactions: transactionsReducer,
  transactionsFilter: transactionsFilterReducer,
  createTagModalReducer,
  editTagModalReducer,
});

export default reducers;
