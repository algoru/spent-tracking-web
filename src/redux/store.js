import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';
import { fetchTransactions } from './actions/transaction.action';
import { fetchTags } from './actions/tag.action';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));

store.dispatch(fetchTransactions({ offset: 0, limit: 15 }));
store.dispatch(fetchTags(0, 999));

export default store;
