import React from 'react';
import './App.css';

import TransactionList from './components/TransactionList';
import Header from './components/Header';
import CreateTag from './components/CreateTag';
import EditTag from './components/EditTag';
import EditTransaction from './components/EditTransaction';
import CreateTransaction from './components/CreateTransaction';

function App() {
  return (
    <div className="app">
      <div className="app-wrapper">
        <Header></Header>
        <TransactionList></TransactionList>

        <CreateTag></CreateTag>
        <CreateTransaction></CreateTransaction>
        <EditTransaction></EditTransaction>
        <EditTag></EditTag>
      </div>
    </div>
  );
}

export default App;
