import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { closeCreateTagModal } from '../../redux/actions';
import { saveTag, setEditingTagName } from '../../redux/actions/tag.action';

import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

function CreateTag() {
  const dispatch = useDispatch();

  const tagsState = useSelector((state) => state.tags);

  const onEditHide = () => {
    dispatch(closeCreateTagModal());
  };

  const handleSaveTag = () => {
    dispatch(saveTag(tagsState.editingTagName));
  };

  const handleTagNameChanged = (event) => {
    const name = event.target.value;
    if (name) {
      dispatch(setEditingTagName(name));
    }
  };

  const getAlertResult = () => {
    if (tagsState.success !== undefined) {
      if (tagsState.success === true) {
        return (
          <Alert variant="success">¡Etiqueta creada satisfactoriamente!</Alert>
        );
      }

      return <Alert variant="danger">¡No se pudo crear la etiqueta!</Alert>;
    }

    return null;
  };

  return (
    <Modal
      onHide={onEditHide}
      show={tagsState.showCreate}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Nueva Etiqueta
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Nombre de la Etiqueta</Form.Label>
            <Form.Control
              onChange={handleTagNameChanged}
              type="text"
              placeholder="Salario, alimentación, renta, etc."
            />
          </Form.Group>
        </Form>
        {getAlertResult()}
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onEditHide}>Cerrar</Button>
        <Button variant="success" onClick={handleSaveTag}>
          Guardar
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default CreateTag;
