import React from 'react';
import './style.css';
import Button from 'react-bootstrap/Button';

import { useDispatch, useSelector } from 'react-redux';
import { openCreateTagModal, openEditTagModal } from '../../redux/actions';
import {
  openCreateTransactionModal,
  setEditingTransactionValue,
  setEditingTransactionTag,
} from '../../redux/actions/transaction.action';
import { setTagToEdit } from '../../redux/actions/tag.action';

function Header() {
  const dispatcher = useDispatch();
  const tagState = useSelector((state) => state.tags);

  const handleOpenEditModal = () => {
    dispatcher(setTagToEdit(tagState.tags[0]));
    dispatcher(openEditTagModal());
  };

  const handleOpenCreateTxModal = (isDeposit) => {
    dispatcher(setEditingTransactionValue(0.0));
    dispatcher(setEditingTransactionTag(tagState.tags[0].id));
    dispatcher(openCreateTransactionModal(isDeposit));
  };

  return (
    <div className="app-header">
      <div className="app-header-title">
        <h2>Spent Tracking</h2>
      </div>
      <div className="app-header-buttons">
        {tagState.tags.length > 0 ? (
          <Button
            variant="success"
            className="mr-2"
            onClick={() => handleOpenCreateTxModal(true)}
          >
            Registrar Ingreso
          </Button>
        ) : null}
        {tagState.tags.length > 0 ? (
          <Button
            variant="danger"
            className="mr-2"
            onClick={() => handleOpenCreateTxModal(false)}
          >
            Registrar Egreso
          </Button>
        ) : null}
        <Button
          variant="info"
          className="mr-2"
          onClick={() => dispatcher(openCreateTagModal())}
        >
          Nueva Etiqueta
        </Button>
        {tagState.tags.length > 0 ? (
          <Button variant="info" onClick={handleOpenEditModal}>
            Editar Etiquetas
          </Button>
        ) : null}
      </div>
    </div>
  );
}

export default Header;
