import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  closeTransactionEditModal,
  setEditingTransactionTag,
  setEditingTransactionValue,
  editTransaction,
  fetchTransactions,
  deleteTransaction,
} from '../../redux/actions/transaction.action';

import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

function EditTransaction() {
  const dispatch = useDispatch();

  const tagsState = useSelector((state) => state.tags);
  const transactionState = useSelector((state) => state.transactions);

  const onTransactionSave = () => {
    dispatch(editTransaction(transactionState.editing));
  };

  const onTransactionValueChange = (event) => {
    if (!isNaN(event.target.value)) {
      const value = +event.target.value;
      dispatch(setEditingTransactionValue(value));
    }
  };

  const onTransactionTagChange = (event) => {
    const tagId = +event.target.value;
    dispatch(setEditingTransactionTag(tagId));
  };

  const showEditForm = () => (
    <Form>
      <Form.Group>
        <Form.Label>Monto</Form.Label>
        <Form.Control
          type="number"
          defaultValue={transactionState.editing.value}
          placeholder="Valor de la transacción"
          onChange={onTransactionValueChange}
        />
        <Form.Text className="text-muted">
          Ingrese el valor en pesos colombianos.
        </Form.Text>
      </Form.Group>

      <Form.Group>
        <Form.Label>Etiqueta</Form.Label>
        <Form.Control as="select" onChange={onTransactionTagChange}>
          {tagsState.tags.map((tag, index) => (
            <option
              key={index}
              value={tag.id}
              selected={tag.id === transactionState.editing.tag.id}
            >
              {tag.name}
            </option>
          ))}
        </Form.Control>
      </Form.Group>
    </Form>
  );

  const onTransactionEditHide = () => {
    dispatch(closeTransactionEditModal());
  };

  const onTransactionDelete = () => {
    dispatch(deleteTransaction(transactionState.editing.id));
    dispatch(fetchTransactions({ offset: 0, limit: 15 }));
    onTransactionEditHide();
  };

  const showOperationResult = () => {
    if (transactionState.success !== undefined) {
      if (transactionState.success === true) {
        return <Alert variant="success">¡Transacción modificada!</Alert>;
      } else {
        return <Alert variant="danger">¡Error durante modificación!</Alert>;
      }
    }

    return null;
  };

  return (
    <Modal
      show={transactionState.show}
      onHide={onTransactionEditHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Editando Transacción
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {transactionState.show ? showEditForm() : null}
        {showOperationResult()}
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onTransactionEditHide}>Cerrar</Button>
        <Button onClick={onTransactionDelete} variant="danger">
          Borrar
        </Button>
        <Button variant="success" onClick={onTransactionSave}>
          Guardar
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default EditTransaction;
