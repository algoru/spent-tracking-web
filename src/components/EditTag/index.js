import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideEditTagModal } from '../../redux/actions';
import {
  setTagToEdit,
  setEditingTagName,
  editTag,
} from '../../redux/actions/tag.action';
import { fetchTransactions } from '../../redux/actions/transaction.action';

import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

function EditTag() {
  const dispatch = useDispatch();
  const tagState = useSelector((state) => state.tags);

  const onEditTagsHide = () => {
    dispatch(hideEditTagModal());
  };

  const onTagChanged = (event) => {
    const tag = JSON.parse(event.target.value);
    dispatch(setTagToEdit(tag));
  };

  const handleTagNameChanged = (event) => {
    const newTagName = event.target.value;
    dispatch(setEditingTagName(newTagName));
  };

  const getAlertResult = () => {
    if (tagState.success !== undefined) {
      if (tagState.success === true) {
        return (
          <Alert variant="success">
            ¡Etiqueta modificada satisfactoriamente!
          </Alert>
        );
      }

      return <Alert variant="danger">¡No se pudo modificar la etiqueta!</Alert>;
    }

    return null;
  };

  const handleTagUpdate = () => {
    dispatch(editTag(tagState.editingTag.id, tagState.editingTagName));
    dispatch(fetchTransactions({ offset: 0, limit: 15 }));
  };

  return (
    <Modal
      onHide={onEditTagsHide}
      show={tagState.showEditTags}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Editar Etiquetas
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>Etiqueta a Editar</Form.Label>
            <Form.Control as="select" onChange={onTagChanged}>
              {tagState.tags.map((tag, index) => (
                <option key={index} value={JSON.stringify(tag)}>
                  {tag.name}
                </option>
              ))}
            </Form.Control>
          </Form.Group>
          {tagState.editingTag !== undefined ? (
            <Form.Group>
              <Form.Label>Nombre de la etiqueta</Form.Label>
              <Form.Control
                type="text"
                placeholder="Salario, comida, renta, etc"
                onChange={handleTagNameChanged}
              />
            </Form.Group>
          ) : null}
        </Form>
        {getAlertResult()}
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onEditTagsHide}>Cerrar</Button>
        <Button variant="success" onClick={handleTagUpdate}>
          Guardar
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default EditTag;
